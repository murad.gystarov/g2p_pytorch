GRAPHEMES = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с',
             'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я', '|', '_']

PHONEMES = ['A', 'A0', 'B', 'B0', 'D', 'D0', 'DZ', 'DZ0', 'DZH', 'DZH0', 'E0', 'F', 'F0', 'G', 'G0', 'GH', 'I',
            'I0', 'J0', 'K', 'K0', 'KH', 'KH0', 'L', 'L0', 'M', 'M0', 'N', 'N0', 'O', 'O0', 'P', 'P0', 'R', 'R0',
            'S', 'S0', 'SH', 'SH0', 'T', 'T0', 'TS', 'TS0', 'TSH', 'TSH0', 'U', 'U0', 'V', 'V0', 'Y', 'Y0', 'Z',
            'Z0', 'ZH', 'ZH0', '|', '_', '[END]', '[START]']