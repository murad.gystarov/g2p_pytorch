import random
import time

import torch
import torch.nn as nn

from graphs.decoder import Decoder
from graphs.encoder import Encoder


class Seq2Seq(nn.Module):
    def __init__(self, device, config_model):
        super().__init__()
        self.encoder = Encoder(config_model['input_dim'], config_model['enc_emb_dim'],
                               config_model['hid_dim'], config_model['n_layers'],
                               config_model['enc_dropout'], config_model['bidirectional'], device=device).to(device)
        self.decoder = Decoder(config_model['output_dim'], config_model['dec_emb_dim'],
                               config_model['hid_dim'], config_model['n_layers'],
                               config_model['dec_dropout'], device=device).to(device)
        self.device = device

        self.time_dec = 0
        self.time_enc = 0

        if self.encoder.bidirectional:
            assert self.encoder.hid_dim * 2 == self.decoder.hid_dim, \
                "Hidden dimensions of encoder and decoder must be equal!"
        else:
            assert self.encoder.hid_dim == self.decoder.hid_dim, \
                "Hidden dimensions of encoder and decoder must be equal!"
        assert self.encoder.n_layers == self.decoder.n_layers, \
            "Encoder and decoder must have equal number of layers!"

    def forward(self, src, trg, teacher_forcing_ratio=0.5):
        # src = [src len, batch size]
        # trg = [trg len, batch size]
        # teacher_forcing_ratio is probability to use teacher forcing
        # e.g. if teacher_forcing_ratio is 0.75 we use ground-truth inputs 75% of the time
        batch_size = trg.shape[1]
        trg_len = trg.shape[0]
        trg_vocab_size = self.decoder.output_dim
        # tensor to store decoder outputs
        outputs = torch.zeros(trg_len, batch_size, trg_vocab_size).to(self.device)

        # last hidden state of the encoder is used as the initial hidden state of the decoder
        st_enc = time.time()
        hidden, cell = self.encoder(src)
        end_enc = time.time()
        self.time_enc += end_enc - st_enc
        # first input to the decoder is the <sos> tokens
        input = trg[0, :]
        start_time_dec = time.time()
        for t in range(1, trg_len):
            # insert input token embedding, previous hidden and previous cell states
            # receive output tensor (predictions) and new hidden and cell states
            output, hidden, cell = self.decoder(input, hidden, cell)
            # place predictions in a tensor holding predictions for each token
            outputs[t] = output
            # decide if we are going to use teacher forcing or not
            teacher_force = random.random() < teacher_forcing_ratio
            # get the highest predicted token from our predictions
            top1 = output.argmax(-1)
            # if teacher forcing, use actual next token as next input
            # if not, use predicted token
            input = trg[t] if teacher_force else top1
        end_time_dec = time.time()
        self.time_dec += end_time_dec - start_time_dec
        return outputs