import time
import sys
from collections import Counter, OrderedDict
import random
from tqdm import tqdm
import torch
from torch.utils.data import IterableDataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
from torchtext.vocab import vocab

import pandas as pd

from data.vocabs import GRAPHEMES, PHONEMES


class G2PDataset:
    """
    класс который создает итерируемый объект
    """

    def __init__(self, path_grapheme_phoneme_sequences):
        self.path = path_grapheme_phoneme_sequences
        self.src_vocab = vocab(OrderedDict([(token, 1) for token in GRAPHEMES]), specials=['[pad]'], special_first=True)
        self.trg_vocab = vocab(OrderedDict([(token, 1) for token in PHONEMES]), specials=['[pad]'], special_first=True)

    @staticmethod
    def line_mapper(line):
        # Splits the line into text and label and applies preprocessing to the text
        src_seq, trg_seq = line.split('\t')
        return src_seq.strip(), trg_seq.strip()

    def __iter__(self):
        # Create an iterator
        # цикл for вызывает метод iter, поэтому при каждом новом запуске цикла for у нас будет считывать файл с нуля
        file_itr = open(self.path)
        mapped_itr = map(self.line_mapper, file_itr)
        return mapped_itr


class G2PDataLoader:
    """
    класс который создает генератор батчей и конвертирует текст в тензор чисел
    """

    def __init__(self, dataset, batch_size, device, batch_first):
        self.dataset = dataset
        self.batch_size = batch_size
        self.device = device
        self.batch_first = batch_first

    def batch_generator(self):
        samples = []
        for src_seq, trg_seq in self.dataset:
            samples.append(((src_seq, trg_seq), len(src_seq)))
            if len(samples) == self.batch_size * 100:
                random.shuffle(samples)
                samples.sort(key=lambda x: x[1])
                samples = [sample[0] for sample in samples]
                for i in range(0, len(samples), self.batch_size):
                    yield self.collate_batch(samples[i:i + self.batch_size])
                samples = []
        if len(samples):
            random.shuffle(samples)
            samples.sort(key=lambda x: x[1])
            samples = [sample[0] for sample in samples]
            for i in range(0, len(samples), self.batch_size):
                yield self.collate_batch(samples[i:i + self.batch_size])

    def collate_batch(self, batch):
        src_list, trg_list = [], []
        for src_seq, trg_seq in batch:
            src_list.append(torch.tensor(self.sequence_to_indices(src_seq, self.dataset.src_vocab)))
            trg_list.append(torch.tensor(self.sequence_to_indices(trg_seq, self.dataset.trg_vocab)))
        src = pad_sequence(src_list, padding_value=0, batch_first=True)
        trg = pad_sequence(trg_list, padding_value=0, batch_first=True)
        if not self.batch_first:
            src = torch.transpose(src, 0, 1)
            trg = torch.transpose(trg, 0, 1)
        return src.to(device=self.device), trg.to(device=self.device)

    @staticmethod
    def sequence_to_indices(sequence, vocab):
        return vocab(sequence.split())

    def __iter__(self):
        return self.batch_generator()


# def batch_sampler():
#     """
#     генератор который выдает батч индексов, для которых sampl-ы по этим индексам имеет одинаковую длину
#     чтобы сгруппировать тексты одинаковой длины вместе, мы случайным образом создаем несколько "пулов"
#     каждый из них имеет размер batch_size * 100. Затем мы сортируем образцы внутри отдельного пула по длине.
#     """
#     indices = [(i, len(src_seq)) for i, (src_seq, trg_seq) in enumerate(dataset)]
#     indices = []
#     for i in range(batch_size * 100):
#         indices.append()
#     random.shuffle(indices)
#     pooled_indices = []
#     # create pool of indices with similar lengths
#     for i in range(0, len(indices), batch_size * 100):
#         pooled_indices.extend(sorted(indices[i:i + batch_size * 100], key=lambda x: x[1]))
#
#     pooled_indices = [index for index, seq_len in pooled_indices]
#     # yield indices for current batch
#     for i in range(0, len(pooled_indices), batch_size):
#         yield pooled_indices[i:i + batch_size]

#
# def print_example_batch(bucket_dataloader):
#     src, trg = next(bucket_dataloader)
#     for src_seq, trg_seq in zip(src, trg):
#         print(src_vocab.lookup_tokens(list(src_seq)))
#         print(trg_vocab.lookup_tokens(list(trg_seq)))


def indicies_to_string(tensor, vocab):
    return [' '.join(vocab.lookup_tokens(list(sample))) for sample in torch.transpose(tensor, 0, 1)]


if __name__ == '__main__':
    path = '/home/murad2/Work/dss/data/g2p/ru_g2p_v5.1.TRAIN'
    batch_size = 128
    dataset = G2PDataset(path)
    # print(len(dataset.src_vocab))
    # print(len(dataset.trg_vocab))
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    print(device)
    dataloader = G2PDataLoader(dataset, batch_size, device=device, batch_first=False)
    # print(type(dataset))
    # print(type(dataloader))
    sources = []
    iter_time = 0
    amount = 1
    start_time = time.time()
    # for elem in tqdm(dataset, file=sys.stdout, total=round(70937930)):
    #     pass
        # if amount * batch_size % 64000 == 0:
        #     print(iter_time / amount)
        #     iter_time = 0
        #     amount = 0
        # end_time = time.time()
        # iter_time += end_time - start_time
        # amount += 1
    for src, trg in tqdm(dataloader, file=sys.stdout, total=round(70937930/batch_size)):
        pass
    #     if amount * batch_size % 64000 == 0:
    #         print("samples=%d %.1f" % (amount * batch_size, iter_time / amount))
    #         iter_time = 0
    #         amount = 0
    #         start_time = time.time()
    #     end_time = time.time()
    #     iter_time += end_time - start_time
    #     amount += 1
        # sources.extend(indicies_to_string(src, dataset.src_vocab))
        # print(src)
        # print(src.shape)
        # print(trg)
        # print(trg.shape)
        # print('')
    # for src in sources:
    #     print(src)
