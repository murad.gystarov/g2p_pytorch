import sys
from datetime import datetime
import yaml
import time
from tqdm import tqdm

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torchinfo import summary

from data_prepare import G2PDataset, G2PDataLoader
from graphs.seq2seq import Seq2Seq
from test import evaluate


def train_one_epoch(model, batch_generator, val_batch_generator, optimizer, criterion, clip,
                    output_dim, batch_size, epoch, path_save_model):
    # setting train mode
    model.train()
    sum_steps_loss_for_epoch = 0
    sum_steps_loss = 0.0
    amount_steps = 0
    buf_1 = 0
    step = 1
    # start_time = time.time()
    # amount train samples 70937930
    for src, trg in tqdm(batch_generator, file=sys.stdout, total=round(1313669/batch_size), ncols=70):
        optimizer.zero_grad()
        output = model(src, trg)

        # trg = [trg sent len, batch size]
        # output = [trg sent len, batch size, output dim]
        # мы для расчет кроссэнтропии не можем подать такие тензоры
        # будем рассматривать каждое слово как отдельный объект и брать пару pred слова и trg слова и уже считать
        # кроссэнтропию для этой пары
        output = output[1:].reshape(-1, output_dim)
        trg = trg[1:].reshape(-1)
        # trg = [(trg sent len - 1) * batch size]
        # output = [(trg sent len - 1) * batch size, output dim]
        loss = criterion(output, trg)
        loss.backward()
        # Let's clip the gradient
        torch.nn.utils.clip_grad_norm_(model.parameters(), clip)

        optimizer.step()
        sum_steps_loss += loss.item()
        amount_steps += 1
        if step * batch_size % 64000 == 0:
            avg_loss = sum_steps_loss / amount_steps
            string_step = "samples=%d loss=%.4f\n" % (step * batch_size, float(avg_loss))
            sum_steps_loss_for_epoch += sum_steps_loss
            sum_steps_loss = 0.0
            log_file.write(string_step)
        if step * batch_size % (64000 * 10) == 0:
            val_loss, acc = evaluate(model, val_batch_generator, criterion, output_dim, batch_size)
            model.train()
            log_file.write("samples=%d val_loss=%.4f acc=%.2f\n" % (step * batch_size, float(val_loss), acc))
            test_file.write("val_loss=%.2f acc=%.2f\n" % (val_loss, acc))
            torch.save({
                'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'loss': loss,
            }, path_save_model)
            log_file.write(f'Saved model {config_train["path_to_save_model"]}\n')
        step += 1
    return sum_steps_loss_for_epoch, step


if __name__ == '__main__':
    experiment_number = '1'

    with open(f'experiments/exp_{experiment_number}/config_train.yaml', 'r') as f:
        config_train = yaml.load(f, Loader=yaml.FullLoader)
    with open(f'experiments/exp_{experiment_number}/metrics.yaml', 'r') as f:
        metrics = yaml.load(f, Loader=yaml.FullLoader)
    with open(config_train['path_to_config_model'], 'r') as f:
        config_model = yaml.load(f, Loader=yaml.FullLoader)
        
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    # device = 'cpu'
    now = datetime.now()
    now_format = now.strftime("%Y_%m_%d_%H:%M")
    log_file = open(f"{config_train['path_to_exp']}/logs/{now_format}.log", 'w', buffering=1)
    test_file = open(f"{config_train['path_to_exp']}/tests/{now_format}.test", 'w', buffering=1)

    print("\nLOAD DATA")
    dataset = G2PDataset(config_train['path_train_data'])
    batch_generator = G2PDataLoader(dataset, config_train['batch_size'], device, batch_first=False)
    val_dataset = G2PDataset(config_train['path_val_data'])
    val_batch_generator = G2PDataLoader(val_dataset, config_train['batch_size'], device, batch_first=False)
    epochs = config_train['epochs']

    print("\nCREATE MODEL")
    config_model['input_dim'] = len(dataset.src_vocab)
    config_model['output_dim'] = len(dataset.trg_vocab)
    with open(config_train['path_to_config_model'], 'w') as f:
        yaml.dump(config_model, f, sort_keys=False)
    model = Seq2Seq(device, config_model).to(device)
    for input_example_batch, target_example_batch in batch_generator:
        print(input_example_batch.shape)
        example_batch_predictions = model(input_example_batch, target_example_batch)
        print(example_batch_predictions.shape)
        summary(model, input_data=(input_example_batch, target_example_batch), device=device)
        break
        # (batch_size, sequence_length, vocab_size)")
    PAD_IDX = dataset.trg_vocab.lookup_indices(['[pad]'])[0]
    optimizer = optim.Adam(model.parameters(), lr=1e-3)
    criterion = nn.CrossEntropyLoss(ignore_index=PAD_IDX)
    if config_train['load_pretrained_model']:
        checkpoint = torch.load(config_train['path_to_save_model'])
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    writer = SummaryWriter()
    for epoch in range(epochs):
        start_time = time.time()
        string_epoch = "\nStart of epoch %d\n" % (epoch,)
        log_file.write(string_epoch)
        loss, amount_steps = train_one_epoch(model, batch_generator, val_batch_generator, optimizer, criterion,
                                             config_train['clip'], config_model['output_dim'],
                                             config_train['batch_size'], epoch, config_train['path_to_save_model'])
        # val_loss, val_amount_steps = evaluate(model, val_batch_generator, criterion, config_model['output_dim'])
        # val_avg_loss = val_loss / val_amount_steps
        # avg_loss_epoch = loss / amount_steps
        # string_epoch_result = "epoch=%d loss_epoch=%.4f val_loss_=%.4f time_epoch=%.1f" % (epoch, avg_loss_epoch,
        #                                                                             val_avg_loss,
        #                                                                             time.time() - start_time)
        # log_file.write(f'{string_epoch_result}\n')
        # torch.save({
        #     'epoch': epoch,
        #     'model_state_dict': model.state_dict(),
        #     'optimizer_state_dict': optimizer.state_dict(),
        #     'loss': loss,
        # }, config_train['path_to_save_model'])
        log_file.write(f'\nSaved model {config_train["path_to_save_model"]}\n\n')
    log_file.close()
    test_file.close()
    #
    # print("\nSAVING MODEL")
    # print(f"   * load last checkpoint")
    # model.load_weights(ckpt_manager.latest_checkpoint)  # загрузка лучшего чекпоинта тоесть последнего реализовать
    # print(f"   * export to: {config_train['path_to_save_model']}")
    # model.save(config_train['path_to_save_model'], save_format="tf")  # мы сохроняем всю модель а не просто веса
    # print(f"   * done!")
    # print("\nFINISHED!")
