import torch
import yaml
import sys
from datetime import datetime

from tqdm import tqdm
import torch
import torch.nn as nn
from torchinfo import summary
from data_prepare import G2PDataset, G2PDataLoader
from graphs.seq2seq import Seq2Seq


def evaluate(model, val_batch_generator, criterion, output_dim, batch_size):
    model.eval()
    val_sum_loss = 0.0
    amount_steps = 0
    count = 0
    match_count = 0
    with torch.no_grad():
        for src, trg in tqdm(val_batch_generator, file=sys.stdout, total=round(1313669/batch_size)):
            output = model(src, trg, 0)  # turn off teacher forcing
            output = output[1:]
            trg = trg[1:]
            for trg_sample, output_sample in zip(torch.transpose(trg, 0, 1), torch.transpose(output, 0, 1)):
                output_sample = list(torch.argmax(output_sample, dim=1))
                if output_sample == trg_sample:
                    match_count += 1
                count += 1
            output = output.reshape(-1, output_dim)
            trg = trg.reshape(-1)
            val_loss = criterion(output, trg)
            val_sum_loss += val_loss
            amount_steps += 1
    acc = float(match_count/count)
    val_avg_loss = float(val_sum_loss/amount_steps)
    return val_avg_loss, acc


if __name__ == '__main__':
    experiment_number = 1
    with open(f'experiments/exp_{experiment_number}/config_train.yaml', 'r') as f:
        config_train = yaml.load(f, Loader=yaml.FullLoader)
    with open(f'experiments/exp_{experiment_number}/metrics.yaml', 'r') as f:
        metrics = yaml.load(f, Loader=yaml.FullLoader)
    with open(config_train['path_to_config_model'], 'r') as f:
        config_model = yaml.load(f, Loader=yaml.FullLoader)

    now = datetime.now()
    now_format = now.strftime("%Y_%m_%d_%H:%M")
    test_file = open(f"{config_train['path_to_exp']}/tests/{now_format}.test", 'w', buffering=1)
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = Seq2Seq(device, config_model).to(device)
    val_dataset = G2PDataset(config_train['path_val_data'])
    val_batch_generator = G2PDataLoader(val_dataset, config_train['batch_size'], device, batch_first=False)
    PAD_IDX = val_dataset.trg_vocab.lookup_indices(['[pad]'])[0]
    criterion = nn.CrossEntropyLoss(ignore_index=PAD_IDX)
    val_loss, acc = evaluate(model, val_batch_generator, criterion, config_model['output_dim'], 16)
    test_file.write("val_loss=%.2f acc=%.2f\n" % (val_loss, acc))
